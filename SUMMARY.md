# Summary

* [GitBook 新手村](README.md)
* [Chrome Extensions](docs/Chrome_Extensions.md)
* [VS Code for Vim mode](docs/VS_Code_for_Vim_Mode.md)
* [處理令人崩潰的 Macbook Pro 2019 Bluetooth Lag 問題](docs/Bluethooth_Lag.md)
* [Sublime Text 4](docs/Sublime_Text_4.md)
* [Vim 系列](docs/Vim.md)
* [Markdown 語法/高亮](docs/Markdown.md)
* [雜項](docs/Misc.md)