# 閃亮亮 Coding 情報站

## Gitbook - baby steps

* `npm install gitbook-cli -g`
* 在 Gitlab create new repo -> `Create from template` -> `Gitbook` (Use template)
* `git clone git@your-repo` 取代 `gitbook init .` 的指令，預設的內容都一樣
* 加入 `book.json` ,ref:
  * <https://gitbookio.gitbooks.io/documentation/content/format/configuration.html>
  * <https://github.com/shihyu/gitbook/blob/master/customize/book.json.md>
* 檢查一下  `.gitlab-ci.yaml` ，啊~~熟悉的感覺，基本不用改什麼。
* 調整預設的 `README.md`　和 `SUMMARY.md` 檔
  * GitBook新手入門 <https://bit.ly/2UGkgNd>
  * ↑ 有用 `Link to Text Fragment` 好用
* 可以用 `gitbook serve` 看本地 generate 出來的靜態頁成果。
  * 瀏覽器打開　<http://localhost:4000>

### 怎麼上圖片？

* 搜尋 `Paset Image` 發現好多個，隨便用了這一個

```txt
Name: Paste Image
Id: mushan.vscode-paste-image
Description: paste image from clipboard directly
Version: 1.0.4
Publisher: mushan
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image
```

* 用 VS Code 搭配 `Paste Image` 好像不錯用，希望預設到一個 `img/` folder 底下(應該可以)
  * [VSCode-Markdown圖片複製插件路徑配置 - 台部落](https://www.twblogs.net/a/5c94ab71bd9eee35cd6be35c)
* 配置：

  ```json
  {    
      "pasteImage.basePath": "${projectRoot}",
      "pasteImage.defaultName": "-MM-DD-HH-mm-ss",
      "pasteImage.path": "${projectRoot}/img",
      "pasteImage.namePrefix": "${currentFileNameWithoutExt}"
  }
  ```

* 待測:
  * 圖床需求：
    * 這篇讚！ [VSCode+PicGo+Github搭建Markdown圖床_leiyu的博客-CSDN博客](https://blog.csdn.net/qq_35621494/article/details/106432399)
    * 上面推薦 `Markdown All in One` VS code plugin, 用 `Github` 做圖床有 100G, 搭 picgo
    * CDN 是 jsdeliver  
  * 之後都改用 VS Code 來編輯，要練習整合 Git, Vim mode 來編輯這文件。

### Preview Markdown

* 在 VS Code 原來有內建: 搜 `markdown preview` 就有
  * HotKey: `Cmd + Shift + V`
  * on the side: `Cmd + K, V`

* 後來更好的做法是
  * 用 `gitbook serve` 開 `http://localhost:4000` 會即時刷新！放螢幕2就行。

### Quesitons

* 圖床：
  * 承上，怎麼設 CDN 啊？要錢嗎？
  * 這樣本地是不是還是最好留一份圖片原檔呢？
* 看來也要研究怎麼快速建 .gif 動圖錄操作
  * [\[Mac\] 螢幕錄製 GIF 工具 Kap 安裝使用教學 (支援 macOS Big Sur) – OneJar 的隧道](https://www.onejar99.com/mac-screen-recording-gif-tool-kap/)
  * `brew install --cask kap` 就完事
  * 好用耶！可以錄 `gif`, `apng`, `webm`, `mp4(h.264)` 結果 mp4 檔案最小
* 要研究怎麼放 mp4 到 markdown 了

```html
<video id="video" controls="" preload="none" poster="http://media.w3.org/2010/05/sintel/poster.png">
      <source id="mp4" src="http://media.w3.org/2010/05/sintel/trailer.mp4" type="video/mp4">
      <source id="webm" src="http://media.w3.org/2010/05/sintel/trailer.webm" type="video/webm">
      <source id="ogv" src="http://media.w3.org/2010/05/sintel/trailer.ogv" type="video/ogg">
      <p>Your user agent does not support the HTML5 Video element.</p>
</video>
```

* 實測一下

<video id="video" controls="" preload="none" poster="http://media.w3.org/2010/05/sintel/poster.png">
      <source id="mp4" src="http://media.w3.org/2010/05/sintel/trailer.mp4" type="video/mp4">
      <source id="webm" src="http://media.w3.org/2010/05/sintel/trailer.webm" type="video/webm">
      <source id="ogv" src="http://media.w3.org/2010/05/sintel/trailer.ogv" type="video/ogg">
      <p>Your user agent does not support the HTML5 Video element.</p>
</video>

### Todo Ideas

* 外掛
  * [x] 最重要的搜尋外掛，搭自訂搜尋引擎來實現知識庫的功能
  * [ ] 整理 .md 到 `docs/`
  * [ ] 整理 website.css 到 `css/` 並自訂文字、標題字大小，甚至樣式
  * TOC in markdown 
  * Sidebar 可以設一些介紹頁
* 變成 .md 的純文字資訊之後，要搜尋之後就很多方法了。整合到 alfred 好像不錯。
* alfred -> `Notes` 就用 VS Code 開專案可以記筆記，也許再加點快速鍵
  * 新建一個文章，一口氣處理好標題和文章連結
  * 快速新增 md 檔的方式
* 建立快速上傳圖片變成 Markdown 流程，同時建本地圖檔備份。

### 心得

* 爽
* 有點懂為何鴻森可以寫這麼多 Blog 筆記了

### 待續的參考資料

* 很完整對於 VS Code 寫 Markdown 各方面都講到，圖文並茂：[一款工具搞定 5 個應用場景：VS Code 上手指南_少數派 - MdEditor](https://www.gushiciku.cn/pl/pLsw/zh-tw)
* [打造完美写作系统：Gitbook+Github Pages+Github Actions_不管过了几载春秋，我还是会偶尔想起-CSDN博客](https://blog.csdn.net/qq_40889820/article/details/110013310)
* [GitBook相關配置及優化 - 台部落](https://www.twblogs.net/a/5d1e10f4bd9eee1e5c835577)
