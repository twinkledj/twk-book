# 設置 VS Code 的 vim mode

## 安裝 `Vim` Plugin

* 簡單，直接搜尋 `Vim` 就有了。
* 左側工具列隨便按按就找到。

## 設置(不斷更新)

* 直接開啟 `settings.json` 而不是 GUI 設定介面:
  * `Cmd` + `Shift` + `p` : `Open Settings (JSON)`
  * 應該設個快速鍵

```json
{
  "vim.statusBarColors.insert": "#87AF5F",
  "vim.statusBarColors.replace": "#870000",
  "vim.statusBarColors.visual": "#ff8c00",
  "vim.statusBarColors.visualblock": "#ff8c00",
  "vim.statusBarColors.visualline": "#ff8c00",
  "vim.neovimPath": "/usr/local/bin/nvim",
  "vim.neovimConfigPath": "~/.config/nvim/init.vim",
  "vim.enableNeovim": true,
  "vim.vimrc.enable": true,
  "vim.leader": " ",
  "editor.lineNumbers": "relative"
}
```

### 設 Relative Line Number

* `"editor.lineNumbers": "relative"`
  * [Relative line numbers in Visual Studio - Stack Overflow](https://stackoverflow.com/questions/4967217/relative-line-numbers-in-visual-studio#:~:text=Since%20VSCode%201.6%20you%20can%20set%20editor.lineNumbers%20with%20the%20value%20of%20relative%20in%20the%20settings.json%20file%20or%20in%20File%3EPreferences%3ESettings.)

## Keymapping Issues  

### 要來頭痛怎麼把 vim 的 key mapping 搬過來了

* `"vim.vimrc.enable": true` in `settings.json`
  * ref: <https://stackoverflow.com/questions/63017771/how-to-modify-change-the-vimrc-file-in-vscode#comment114502642_63030352>
* 搞定！基本成功

### 我需要 Ctrl + Tab 綁 Cmd + E 來回切換最近開的檔案

* `Keyboard Shortcuts` 在左下角齒輪
  * `Cmd + K, Cmd + S`
  * 搜尋 `Ctrl + Tab` 可以直接找到想要的功能還不錯！
  * 無法自動移除重複的 Keybindings 像 PhpStorm 那樣
  * 綁完 `Cmd + E` 後，發現不能連按，不像 `Ctrl + Tab` 可以一直切檔
  * 既然綁了 `CapsLock` 成 `Esc` 加 `Ctrl` 複合體，那也許不用 Cmd + e 了吧，畢竟 VS Code 和 Sublime Text, PhpStorm 都能用
  * 而且在 PhpStorm 還要按 `<Enter>` 才能切檔
* ====>> 習慣改用 `Ctrl + Tab` (`CapsLock + Tab`) 切檔

### 我要 Cmd + 1 把 VS Code 側邊欄 Toggle => `Cmd + K, Cmd + B`

* 正常是 `Cmd + B`
* 但 Markdown All In One 有 `bold` Text 功能也是 `Cmd + B`
* 查 `Keyboard Shortcuts` : `Cmd + B` 發現可以用 `Cmd + K, Cmd + B`
* Done. It works well~
* 先用一陣子再整合 Sublime, PhpStorm 的 Cmd + 1 吧

### 崩潰到想用 Sublime 的 Cmd + N (New File) in VS Code

* 為什麼 `新增檔案` 不能 `貼上` 想新增的檔名啦！ 無法貼上檔名。
* 太好了，這就是我需要的：
  * 開 `keybinds.json` 搜 `key json`
  * 貼這段，完美好用，直接側欄開新檔，可以貼檔名，還看得到在什麼目錄下。

```json
{
  "key": "cmd+n",
  "command": "-workbench.action.files.newUntitledFile"
},
{
  "key": "cmd+n",
  "command": "explorer.newFile"
}
},
{
  "key": "cmd+shift+n",
  "command": "explorer.newFolder"
}
```

### Easymotion 一開始設完 `"vim.vimrc.enable": true` 不能用

* Ex: `nmap zf          <Plug>(easymotion-bd-w)` 是不能用的
* 先亂試裝個 `EasyMotion` 在 plugin marketplace => 直接搜尋找不到
  * ref: <https://marketplace.visualstudio.com/items?itemName=JaredParMSFT.EasyMotion>
  * 直接下載 vsix 檔，還查了怎麼裝 vsix
* 結果: 不能用！
* Error:

```bash
Command 'Extensions: Install from VSIX...' resulted in an error (extension/package.json not found inside zip.)
```

* 再找辦法試：
  * 有點太手動設置了，我一堆鍵： [VSCODE Vim+easymotion 配置. 最近入了 HHKB BT 2 无刻版，使用一周感觉非常爽，薄膜键盘的触感 +… | by JohnnyLau | Medium](https://medium.com/@realjohnnylau/vscode-vim-easymotion-%E9%85%8D%E7%BD%AE-6b64bba642cf)
  * 好像慢慢來可能比較快，手動設我大概 5 個 keybindings 就搞定
  * 想找讓 `<Plug>(easymotion-bd-w)` 可以作用的方法
  * 找不到，放棄！直接設對應的 Key

* From `.vimrc`

```vimrc
nmap z<space>    <Plug>(easymotion-bd-f)
nmap ze          <Plug>(easymotion-bd-E)
nmap zf          <Plug>(easymotion-bd-w)
map  <leader>j    <Plug>(easymotion-bd-f)
map  <leader>e    <Plug>(easymotion-bd-E)
map  <leader>/    <Plug>(easymotion-bd-w)
```

* 相關好文：
  * 這作者風格我喜歡 版面也漂亮 [聊聊你在 vim 常用的移動方式 | no code no pain](https://amikai.github.io/2020/10/03/vim-commonly-used-motion/)
  * 發現上篇這種是認真寫的，我這種是隨手記錄的。

### 安裝 .vsix 檔

* `Cmd` + `Shift` + `p` : 找 `VSIX` 就行了
  * 好吧，認命再打一次完整的 `Extensions: Install from VSIX`
  * Command id: `workbench.extensions.action.installVSIX`

* ref: [尋找及安裝延伸模組 - Visual Studio | Microsoft Docs](https://docs.microsoft.com/zh-tw/visualstudio/ide/finding-and-using-visual-studio-extensions?view=vs-2019)
  * 英文版: [Find and install extensions - Visual Studio | Microsoft Docs](https://docs.microsoft.com/en-us/visualstudio/ide/finding-and-using-visual-studio-extensions?view=vs-2019)
  * `Extensions > Manage Extensions`
  * 結果我找不到上面教怎麼用。

## 一些 VS Code 的建議設定

* 待整理
* [VS Code settings you should customize - DEV Community](https://dev.to/softwaredotcom/community-showcase-automated-home-setup-to-stay-in-flow-37f)
* [擁抱 Vim 讓你的時間不浪費在編輯上. 不得不說，其實我在 Vim… | by Guy | Guy Chien | Medium](https://medium.com/guy-chien/%E6%93%81%E6%8A%B1-vim-%E8%AE%93%E4%BD%A0%E7%9A%84%E6%99%82%E9%96%93%E4%B8%8D%E6%B5%AA%E8%B2%BB%E5%9C%A8%E7%B7%A8%E8%BC%AF%E4%B8%8A-f557d8e3e87e)
  
## Todos Idea

* Cmd + K, Cmd + V 開啟 markdown preview 同時把 sidebar 關閉 (Cmd + K, Cmd + B)
