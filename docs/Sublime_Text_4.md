# Sublime Text 4 

## 把 .vimrc 的 keymapping 塞進 Sublime Text

* TBD
* 主要解決 zh, zl 不能移動的問題

## 哪招，Vim mode 按住 j, k 居然不能移動了

* 沒有 repeat key, 只會移動一行，待解。2021-0626
* 也好，逼我用 3j, 4k 來跳就對了，先暫不處理試試看。
