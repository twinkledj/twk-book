# Chrome Extensions

## FormatLink

* 商店連結: <https://chrome.google.com/webstore/detail/format-link/pocemhmkmchpgamlnocemnbhlcjcbjgg>
* 需求:
  * 要可以客製我要的 Markdown link 結果
  * 試過幾個都太過陽春，或是文件不清，不夠 Hack

## Link to Text Fragment

* 商店連結: <https://chrome.google.com/webstore/detail/link-to-text-fragment/pbcodcjpfjdpcineamnnmbkkmkdpajjg>
* 好用！
  * 拿到連結的人，不用裝這個外掛也能有效果
  * 可以直接跳轉到 hightlight 處的文字，存書籤時好用！

### 相關功能(內建於 Chrome 90) 好像是變成內建功能，不用裝 extension

* 可以不用 `Link to Text Fragment` 靠內建功能做到一樣的事。
* ref: <https://www.aboutchromebooks.com/news/how-to-use-the-copy-link-to-text-option-in-chrome-os-90/>
* Open in Chrome: `chrome://flags/#copy-link-to-text`
* 選 `Enable`，要重啟 Chrome
* 右鍵下拉選單中(帶圖TBD) 應該會出現 `Copy link to hightlight`
* 2021/6/26 16:00 測試ok，長出來的 link 和 Link to Text Fragment 基本上好像一樣。

## `Copy Link Name` 功能異動

* 先猜是因為升級 Chrome 90 的關係
* Copy Link Name, Copy Link Text 的外掛好像都失效了
* 取代方案：
  * Chrome version: `Version 91.0.4472.114 (Official Build) (x86_64)`
  * 右鍵 click 似乎會自動選取 link name 直接選 copy 就行了
