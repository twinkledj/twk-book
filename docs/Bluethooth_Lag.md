# 處理令人崩潰的 Macbook Pro 2019 Bluetooth Lag 問題

## 發生情況

* 一覺醒來，再開 Bluetooth 連 speaker 就出現斷續的狀況，型號備註：`Creative T15`
* 使用 `AirPod Pro` 連也一樣
* 用任何播放器也一樣，Ex: `IINA`
* 用內建的 Speaker 不會發生 lag 情況
* 聽起來很像被干擾？ 但是重開機之後又沒事了

# "我不想要一直重開機啊啊啊~~~"

## 已嘗試過的(無效)的相關指令，我不想一直重開機

* `sudo launchctl kickstart -kp system/com.apple.audio.coreaudiod`
  * 討論串: <https://gist.github.com/felipecsl/5177790>
* `sudo killall coreaudiod`
  * 功能應該同上

## 待測解決方式

* [居然要砍掉 Android File Transfer !?](https://pc-mac-help.com/blog/fix-bluetooth-not-working-after-wake-from-sleep-on-macos-mojave)
  * 先不砍，下次測時來砍

* [如何重置 Mac 的 SMC - T2 chip (僅適用於配備 Intel 處理器的 Mac 電腦)](https://support.apple.com/zh-tw/HT201295)
  * 關機，開機時按下 `Ctrl(左)` + `Option(alt)(左)` + `Shift(要右邊的)` 按住 7 secs
  * 電源鍵(指紋辨識) 按住再 7 secs
  * 聽到聲音為止?

* [重置 Mac 上的 NVRAM 或 PRAM - Apple 支援](https://support.apple.com/zh-tw/HT204063)
  * 開機並立即按住這四個按鍵： `Option` + `Command` + `P` + `R` 。約 20 秒後可以放開按鍵
  * 在配備 Apple T2 安全晶片的 Mac 電腦上，您可以在 Apple 標誌第二次出現又消失之後，放開按鍵。

## 節外生技: 怎麼開啟 Macbook AptX 支援

* <https://www.youtube.com/watch?v=E0WhVIg8n6o>
  * 有個大寫的 `Codec` 那是打錯的，害我也打錯一遍。
  * `sudo defaults read bluetoothaudiod`
  * `sudo defaults write bluetoothaudiod "Enable AptX codec" -bool true`
  * `sudo defaults write bluetoothaudiod "Enable AAC codec" -bool true`

* Option + Click 狀態列 藍芽圖案
