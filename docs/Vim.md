# Vim

## Autocmd 自動 reload .vimrc 之後的問題

* 會自動 Fold 無法抵達編輯位置，因為有設 `" vim: foldmethod=marker:foldlevel=0` 在 .vimrc 最底
* 加上有自動 source 的 script

```vim
if !has("gui_running")
    augroup autosourcing
        autocmd!
        autocmd BufWritePost .vimrc source %
        autocmd BufWritePre * :%s/\s\+$//e
    augroup END
endif
```

### 讓 `.vimrc` 自動回到最後編輯的位置成功，但要打開 Folding

* 外掛:
  * <https://github.com/farmergreg/vim-lastplace>
* 自己掛:
  * Vim Tips Wiki: <https://vim.fandom.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session>
  * <https://askubuntu.com/a/224908>
* 土炮：

```vim
if !has("gui_running")
    augroup autosourcing
        autocmd!
        autocmd BufWritePost .vimrc source %
        " 加入下面這行 👇👇👇
        autocmd BufReadPost  .vimrc normal! g;
        autocmd BufWritePre * :%s/\s\+$//e
    augroup END
endif
" 如果目前檔案是 .vimrc 就移到最後編輯位置，如果有 fold 就 zo 打開
" 加入下面這段 👇👇👇
if ".vimrc" == expand('%:t')
    if &foldenable && foldlevel(line(".")) > 0
        normal! zo
    endif
endif
```

### 延伸問題：markdown preview 有 syntax color, 但在 VS Code 裡沒有 (附圖)

\# 如圖: ``` 中間的區塊，沒有顏色

![Vim-Fenced-Snapshot](/img/Vim-06-27-00-12-30.png)
