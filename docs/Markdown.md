# Markdown 

## 高亮備忘 Markdown Syntax Highlight 

### Refs:
  * [Markdown 中支援高亮度顯示的程式語言 | ShunNien's Blog](https://shunnien.github.io/2015/11/19/Syntax-highlighting-in-markdown/)
  * [Syntax highlighting in markdown](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)

### 特殊標註：
  * `vim`
  * `ini`
  * `markdown`
  * `bash` 記得也有 `zsh` 的樣子
  * `diff`
  * `smarty` 居然有

### Syntax Supported - 把一些沒見過的砍掉:

```
Cucumber ('*.feature')
ada ('*.adb', '*.ads', '*.ada')
ahk ('*.ahk', '*.ahkl')
apacheconf ('.htaccess', 'apache.conf', 'apache2.conf')
applescript ('*.applescript')
bash ('*.sh', '*.ksh', '*.bash', '*.ebuild', '*.eclass')
bat ('*.bat', '*.cmd')
c ('*.c', '*.h')
cmake ('*.cmake', 'CMakeLists.txt')
coffeescript ('*.coffee')
console ('*.sh-session')
cpp ('*.cpp', '*.hpp', '*.c++', '*.h++', '*.cc', '*.hh', '*.cxx', '*.hxx', '*.pde')
csharp ('*.cs')
css ('*.css')
diff ('*.diff', '*.patch')
erb ('*.erb')
erlang ('*.erl', '*.hrl')
factor ('*.factor')
fortran ('*.f', '*.f90')
gnuplot ('*.plot', '*.plt')
go ('*.go')
haml ('*.haml')
haskell ('*.hs')
html ('*.html', '*.htm', '*.xhtml', '*.xslt')
ini ('*.ini', '*.cfg')
irc ('*.weechatlog')
jade ('*.jade')
java ('*.java')
js ('*.js')
jsp ('*.jsp')
lua ('*.lua', '*.wlua')
make ('*.mak', 'Makefile', 'makefile', 'Makefile.*', 'GNUmakefile')
mason ('*.mhtml', '*.mc', '*.mi', 'autohandler', 'dhandler')
markdown ('*.md')
perl ('*.pl', '*.pm')
php ('*.php', '*.php(345)')
postscript ('*.ps', '*.eps')
prolog ('*.prolog', '*.pro', '*.pl')
properties ('*.properties')
py3tb ('*.py3tb')
pytb ('*.pytb')
python ('*.py', '*.pyw', '*.sc', 'SConstruct', 'SConscript', '*.tac')
rb ('*.rb', '*.rbw', 'Rakefile', '*.rake', '*.gemspec', '*.rbx', '*.duby')
rebol ('*.r', '*.r3')
rhtml ('*.rhtml')
rst ('*.rst', '*.rest')
sass ('*.sass')
scala ('*.scala')
scheme ('*.scm')
scss ('*.scss')
smarty ('*.tpl')
sourceslist ('sources.list')
sql ('*.sql')
sqlite3 ('*.sqlite3-console')
squidconf ('squid.conf')
tcl ('*.tcl')
tcsh ('*.tcsh', '*.csh')
tex ('*.tex', '*.aux', '*.toc')
text ('*.txt')
v ('*.v', '*.sv')
vbnet ('*.vb', '*.bas')
vim ('*.vim', '.vimrc')
xml ('*.xml', '*.xsl', '*.rss', '*.xslt', '*.xsd', '*.wsdl')
xslt ('*.xsl', '*.xslt')
yaml ('*.yaml', '*.yml')
```